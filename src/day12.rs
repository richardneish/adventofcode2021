use std::collections::BTreeSet;
use std::io;

pub fn day_12a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let lines = input_reader.lines().map(|l| l.unwrap());
    //    let mut graph = Graph::new();
    let mut start_nodes: BTreeSet<String> = BTreeSet::new();
    let mut end_nodes: BTreeSet<String> = BTreeSet::new();
    let mut paths: BTreeSet<(String, String)> = BTreeSet::new();
    for line in lines {
        let line = line.trim();
        let mut tokens = line.split("-");
        match tokens.next() {
            Some("start") => {
                start_nodes.insert(tokens.next().unwrap().to_string());
            }
            Some("end") => {
                end_nodes.insert(tokens.next().unwrap().to_string());
            }
            Some(node_name) => {
                paths.insert((node_name.to_string(), tokens.next().unwrap().to_string()));
            }
            None => (),
        }
    }

    let mut path_count = 0;

    path_count
}

//#[derive(Debug, PartialEq, PartialOrd, Ord, Eq)]
//struct Node {
//    name: String,
//    is_big: bool,
//}
//
//impl Node {
//    fn new(node_name: String) -> Node {
//        let mut node_is_big: bool;
//        if node_name.chars().all(|ch| ch.is_uppercase()) {
//            node_is_big = true;
//        } else if node_name.chars().all(|ch| ch.is_lowercase()) {
//            node_is_big = false;
//        } else {
//            panic!("Invalid node name '{:?}'", node_name);
//        }
//
//        Node {
//            name: node_name,
//            is_big: node_is_big,
//        }
//    }
//}

//impl Ord for Node {
//    fn cmp(&self, other: &Self) -> ordering {
//        self.name.cmp(&other.name)
//    }
//}

//#[derive(Debug)]
//struct Graph {
//    nodes: BTreeSet<Node>,
//    paths: BTreeSet<(Node, Node)>,
//}
//
//impl Graph {
//    fn new() -> Graph {
//        Graph {
//            nodes: BTreeSet::new(),
//            paths: BTreeSet::new(),
//        }
//    }
//
//    fn add_start_node(&self, node_name: String) {
//        let node = Node::new(node_name);
//        if !self.nodes.contains(&node) {
//            self.nodes.insert(node);
//        }
//    }
//
//    fn add_end_node(&self, node_name: String) {
//        let node = Node::new(node_name);
//        if !self.nodes.contains(&node) {
//            self.nodes.insert(node);
//        }
//    }
//
//    fn add_path(&self, start_node_name: String, end_node_name: String) {
//        let start_node = Node::new(start_node_name);
//        if !self.nodes.contains(&start_node) {
//            self.nodes.insert(start_node);
//        }
//        let end_node = Node::new(end_node_name);
//        if !self.nodes.contains(&end_node) {
//            self.nodes.insert(end_node);
//        }
//        self.paths.insert((start_node, end_node));
//    }
//}

//#[test]
fn test_day_12a_small() {
    let input = b"start-A
        start-b
        A-c
        A-b
        b-d
        A-end
        b-end";
    assert_eq!(10, day_12a(&input[..]));
}
