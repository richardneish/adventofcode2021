use std::io;
pub fn day_2a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut horizontal_position = 0;
    let mut depth = 0;
    for line in input_reader.lines() {
        let line = line.unwrap();
        let words: &Vec<&str> = &line[..].split_whitespace().collect();
        println!("{:?}", words);

        match words[..] {
            [direction, amount_as_string] => {
                let amount = amount_as_string.parse::<i32>().unwrap();
                match direction {
                    "forward" => horizontal_position += amount,
                    "down" => depth += amount,
                    "up" => depth -= amount,
                    _ => panic!("Unknown direction '{:?}'", direction),
                }
            }
            _ => panic!("Expected 2 arguments, got '{:?}'", words),
        }
    }
    horizontal_position * depth
}

pub fn day_2b<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut horizontal_position = 0;
    let mut depth = 0;
    let mut aim = 0;
    for line in input_reader.lines() {
        let line = line.unwrap();
        let words: &Vec<&str> = &line[..].split_whitespace().collect();
        println!("{:?}", words);

        match words[..] {
            [direction, amount_as_string] => {
                let amount = amount_as_string.parse::<i32>().unwrap();
                match direction {
                    "forward" => {
                        horizontal_position += amount;
                        depth += aim * amount;
                    }
                    "down" => aim += amount,
                    "up" => aim -= amount,
                    _ => panic!("Unknown direction '{:?}'", direction),
                }
            }
            _ => panic!("Expected 2 arguments, got '{:?}'", words),
        }
    }
    horizontal_position * depth
}

#[test]
fn test_day_2a() {
    let input = b"forward 5
down 5
forward 8
up 3
down 8
forward 2
";
    assert_eq!(150, day_2a(&input[..]));
}

#[test]
fn test_day_2b() {
    let input = b"forward 5
down 5
forward 8
up 3
down 8
forward 2
";
    assert_eq!(900, day_2b(&input[..]));
}
