use std::collections::{BTreeMap, BTreeSet};
use std::io;

pub fn day_8a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let lines = input_reader.lines().map(|l| l.unwrap());
    let mut interesting_digit_count = 0;
    for line in lines {
        let line = line.trim();
        // println!("line: {:?}", line);
        let fields: Vec<&str> = line.split("|").collect();
        let signal_patterns: Vec<&str> = fields[0].split_whitespace().collect();
        let output_values: Vec<&str> = fields[1].split_whitespace().collect();
        // println!(
        //     "signal_patterns: {:?}, output_value: {:?}",
        //    signal_patterns, output_value
        // );

        for output_value in output_values {
            interesting_digit_count += match output_value.len() {
                2 | 4 | 3 | 7 => 1,
                _ => 0,
            }
        }
        //        let mut interesting_digits = BTreeMap::<u8, &str>::new();
        //        for signal in signal_patterns {
        //            if signal.len() == 2 {
        //                interesting_digits.insert(1, signal);
        //            } else if signal.len() == 4 {
        //                interesting_digits.insert(4, signal);
        //            } else if signal.len() == 3 {
        //                interesting_digits.insert(7, signal);
        //            } else if signal.len() == 7 {
        //                interesting_digits.insert(8, signal);
        //            }
        //        }
        //        println!("interesting_digits: {:?}", interesting_digits);
    }
    interesting_digit_count
}

/*
 * Assign segments of the 7-segment display as follows:
 *  0000
 * 1    2
 * 1    2
 *  3333
 * 4    5
 * 4    5
 *  6666
 *
 * Using this system, the mappings from digit to elements is:
 * 0: 0, 1, 2,    4, 5, 6  (6 elements)
 * 1:       2,       5     (2 elements)
 * 2: 0,    2, 3, 4,    6  (5 elements)
 * 3: 0,    2, 3,    5, 6  (5 elements)
 * 4:    1, 2, 3,    5     (4 elements)
 * 5: 0, 1,    3,    5, 6  (5 elements)
 * 6: 0, 1,    3, 4, 5, 6  (6 elements)
 * 7: 0,    2,       5     (3 elements)
 * 8: 0, 1, 2, 3, 4, 5, 6  (7 elements)
 * 9: 0, 1, 2, 3,    5, 6  (6 elements)
 *
 * Inversely the mapping from elements to digits containing that element is:
 * 0: 0,    2, 3,    5, 6, 7, 8, 9
 * 1: 0,          4, 5, 6,    8, 9
 * 2: 0, 1, 2, 3, 4,       7, 8, 9
 * 3:       2, 3, 4, 5, 6,    8, 9
 * 4: 0,    2,          6,    8
 * 5: 0, 1,    3, 4, 5, 6, 7, 8, 9
 * 6: 0,    2, 3,    5, 6,    8, 9
 *
 * Given the puzzle input, work out which letter corresponds to each element,
 * then calculate the sum of the output values.
 */
pub fn day_8b<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let lines = input_reader.lines().map(|l| l.unwrap());
    let mut sum_of_digits = 0;
    for line in lines {
        let line = line.trim();
        // println!("line: {:?}", line);
        let fields: Vec<&str> = line.split("|").collect();
        let signal_patterns: Vec<&str> = fields[0].split_whitespace().collect();
        let output_values: Vec<&str> = fields[1].split_whitespace().collect();
        // println!(
        //     "signal_patterns: {:?}, output_value: {:?}",
        //    signal_patterns, output_value
        // );

        let mut elements: [char; 7] = compute_elements_for_digits(signal_patterns);
        println!("Element 0: {:?}", elements[0]);
    }
    sum_of_digits
}

fn compute_elements_for_digits(signal_patterns: Vec<&str>) -> [char; 7] {
    let mut element: [char; 7] = [' '; 7];
    let mut elements_for_digit: [BTreeSet<char>; 10] = [
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
        BTreeSet::new(),
    ];
    for signal in &signal_patterns {
        let signal_elements: BTreeSet<char> = signal.chars().collect();
        if signal.len() == 2 {
            elements_for_digit[1] = signal_elements;
        } else if signal.len() == 4 {
            elements_for_digit[4] = signal_elements;
        } else if signal.len() == 3 {
            elements_for_digit[7] = signal_elements;
        } else if signal.len() == 7 {
            elements_for_digit[8] = signal_elements;
        }
    }
    element[0] = *(elements_for_digit[7]
        .difference(&(elements_for_digit[1]))
        .next()
        .unwrap());

    // elements for 0 = 6 element signal that does not overlap 1
    let mut signals_with_6_elements = signal_patterns.clone();
    signals_with_6_elements.retain(|&p| p.len() == 6);
    for signal in signals_with_6_elements {
        let signal_elements: BTreeSet<char> = signal.chars().collect();
        if (elements_for_digit[1]).difference(&signal_elements).count() == 0 {
            elements_for_digit[0] = signal_elements;
        }
    }

    element[3] = *(elements_for_digit[8]
        .difference(&(elements_for_digit[0]))
        .next()
        .unwrap());

    // elements for 3 are element 0, 2, 3, 5, 6.  We know 0 & 3, and 2 &5 are in elemen1

    element
}

#[test]
fn test_day_8a() {
    let input =
        b"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
          edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
          fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
          fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
          aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
          fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
          dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
          bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
          egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
          gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
    assert_eq!(26, day_8a(&input[..]));
}

//#[test]
fn test_day_8b() {
    let input =
        b"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
          edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
          fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
          fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
          aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
          fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
          dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
          bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
          egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
          gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
    assert_eq!(61229, day_8b(&input[..]));
}

#[test]
fn test_day_8b_element0() {
    let signal_patterns = vec![
        "acedgfb", "cdfbe", "gcdfa", "fbcad", "dab", "cefabd", "cdfgeb", "eafb", "cagedb", "ab",
    ];
    let elements = compute_elements_for_digits(signal_patterns);
    //assert_eq!(['d', 'e', 'a', 'f', 'g', 'b', 'c'], elements);
    assert_eq!('d', elements[0]);
    assert_eq!('f', elements[3]);
}
