use std::io;
pub fn day_3a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut zero_bits: Vec<u32> = Vec::new();
    let mut one_bits: Vec<u32> = Vec::new();
    for line in input_reader.lines() {
        let line = line.unwrap();
        for pos in 0..line.len() {
            if pos == zero_bits.len() {
                zero_bits.resize(pos + 1, 0);
            }
            if pos == one_bits.len() {
                one_bits.resize(pos + 1, 0);
            }
            let chars = line.chars().collect::<Vec<char>>();
            let c = chars.get(pos).unwrap();
            match c {
                '0' => zero_bits[pos] += 1,
                '1' => one_bits[pos] += 1,
                _ => panic!("Unexpected digit '{:?}'", c),
            }
        }
    }
    if zero_bits.len() != one_bits.len() {
        panic!("Invalid state!");
    }
    let mut gamma = 0;
    let mut epsilon = 0;
    for pos in 0..zero_bits.len() {
        gamma *= 2;
        epsilon *= 2;
        if one_bits[pos] > zero_bits[pos] {
            gamma += 1;
        }
        if one_bits[pos] < zero_bits[pos] {
            epsilon += 1;
        }
    }
    println!("gamma: {:?}, epsilon: {:?}", gamma, epsilon);
    gamma * epsilon
}

pub fn day_3b<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut zero_bits: Vec<u32> = Vec::new();
    let mut one_bits: Vec<u32> = Vec::new();
    let mut o2_gen_rating: Vec<String> = Vec::new();
    let mut co2_scrubber_rating: Vec<String> = Vec::new();
    let lines = input_reader.lines();
    for line in lines {
        let line = line.unwrap();
        o2_gen_rating.push(line.clone());
        co2_scrubber_rating.push(line.clone());
        for pos in 0..line.len() {
            if pos == zero_bits.len() {
                zero_bits.resize(pos + 1, 0);
            }
            if pos == one_bits.len() {
                one_bits.resize(pos + 1, 0);
            }
            let chars = line.chars().collect::<Vec<char>>();
            let c = chars.get(pos).unwrap();
            match c {
                '0' => zero_bits[pos] += 1,
                '1' => one_bits[pos] += 1,
                _ => panic!("Unexpected digit '{:?}'", c),
            }
        }
    }
    if zero_bits.len() != one_bits.len() {
        panic!("Invalid state!");
    }
    for pos in 0..zero_bits.len() {
        if one_bits[pos] > zero_bits[pos] {}
        if one_bits[pos] < zero_bits[pos] {}
    }
    42
}

#[test]
fn test_day_3a() {
    let input = b"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
    assert_eq!(198, day_3a(&input[..]));
}

//#[test]
fn test_day_3b() {
    let input = b"00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";
    assert_eq!(230, day_3b(&input[..]));
}
