use std::collections::BTreeSet;
use std::io;

pub fn day_11a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut grid: [[u8; 10]; 10] = [[0; 10]; 10];
    let lines = input_reader.lines().map(|l| l.unwrap());
    let mut row = 0;
    for line in lines {
        let line = line.trim();
        //println!("line: {:?}", line);
        let mut col = 0;
        for item in line.bytes() {
            let value = item - 48;
            grid[row][col] = value;
            col += 1;
        }
        row += 1;
    }
    let boxed_grid = Box::<[[u8; 10]; 10]>::from(grid);
    println!("Grid: {:?}", boxed_grid);
    for step in 1..=10 {
        println!("Step: {:?}", step);
    }
    42
}

//#[test]
fn test_day_11a() {
    let input = b"5483143223
        2745854711
        5264556173
        6141336146
        6357385478
        4167524645
        2176841721
        6882881134
        4846848554
        5283751526";
    assert_eq!(1656, day_11a(&input[..]));
}
