extern crate adventofcode2021;

use adventofcode2021::day1;
use adventofcode2021::day10;
use adventofcode2021::day14;
use adventofcode2021::day2;
use adventofcode2021::day3;
use adventofcode2021::day8;
use std::env;
use std::io;

fn main() {
    let args: Vec<String> = env::args().collect();

    let command = &args[1];
    match &command[..] {
        "day_1a" => println!("Output: {:?}", day1::day_1a(io::stdin().lock())),
        "day_1b" => println!("Output: {:?}", day1::day_1b(io::stdin().lock())),
        "day_2a" => println!("Output: {:?}", day2::day_2a(io::stdin().lock())),
        "day_2b" => println!("Output: {:?}", day2::day_2b(io::stdin().lock())),
        "day_3a" => println!("Output: {:?}", day3::day_3a(io::stdin().lock())),
        "day_8a" => println!("Output: {:?}", day8::day_8a(io::stdin().lock())),
        "day_10a" => println!("Output: {:?}", day10::day_10a(io::stdin().lock())),
        "day_10b" => println!("Output: {:?}", day10::day_10b(io::stdin().lock())),
        "day_14a" => println!("Output: {:?}", day14::day_14a(io::stdin().lock())),
        "day_14b" => println!("Output: {:?}", day14::day_14b(io::stdin().lock())),
        _ => panic!("Unknown command '{:?}'", command),
    }
}
