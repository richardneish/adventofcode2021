use std::io;
pub fn day_7a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut lines = input_reader.lines().map(|l| l.unwrap());
    let first_line = lines.next();
    for line in lines {
        let line = line.trim();
    }
    42
}

//#[test]
fn test_day_7a() {
    let input = b"16,1,2,0,4,2,7,1,2,14";
    assert_eq!(37, day_7a(&input[..]));
}
