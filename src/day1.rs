use std::io;

pub fn day_1a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut previous_depth = i32::MAX;
    let mut increase_count = 0;

    for line in input_reader.lines() {
        let line = line.unwrap();
        println!("{:?}", line);
        let current_depth = line.parse::<i32>().unwrap();
        if current_depth > previous_depth {
            increase_count += 1;
        }
        previous_depth = current_depth;
    }
    increase_count
}

pub fn day_1b<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    const WINDOW_SIZE: usize = 3;

    let mut depths = Vec::new();
    let mut increase_count = 0;

    for line in input_reader.lines() {
        let line = line.unwrap();
        println!("{:?}", line);
        let depth = line.parse::<i32>().unwrap();
        depths.push(depth);
    }

    let mut previous_depth = i32::MAX;
    for current_depths in depths.windows(WINDOW_SIZE) {
        println!("current_depths: {:?}", current_depths);
        let mut current_depth = 0;
        for d in current_depths.iter() {
            current_depth += d;
        }
        if current_depth > previous_depth {
            increase_count += 1;
        }
        previous_depth = current_depth;
    }
    increase_count
}

#[test]
fn test_day_1a() {
    let input = b"199
200
208
210
200
207
240
269
260
263
";

    assert_eq!(7, day_1a(&input[..]));
}

#[test]
fn test_day_1b() {
    let input = b"199
200
208
210
200
207
240
269
260
263
";
    assert_eq!(5, day_1b(&input[..]));
}
