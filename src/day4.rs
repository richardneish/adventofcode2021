use std::io;
pub fn day_4a<R>(input_reader: R) -> i32
where
    R: io::BufRead,
{
    let mut lines = input_reader.lines().map(|l| l.unwrap());
    let first_line = lines.next();
    let mut boards = Vec::new();
    for line in lines {
        let line = line.trim();
        let mut board = BingoBoard::new();
        if line.len() == 0 {
            if board.is_complete {
                println!("Adding new board: {:?}", board);
                boards.push(board);
            }
            board = BingoBoard::new();
            continue;
        }
        //        board.add_row(line.split_whitespace().collect());
    }
    42
}

#[derive(Debug)]
struct BingoBoard {
    is_complete: bool,
    rows: Vec<Vec<String>>,
}

impl BingoBoard {
    pub fn new() -> BingoBoard {
        BingoBoard {
            is_complete: false,
            rows: Vec::new(),
        }
    }

    //    pub fn add_row(self, items: Vec<str>) {
    //        self.rows.push(items);
    //    }
}

#[test]
fn test_day_4a() {
    let input = b"7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";
    //    assert_eq!(4512, day_4a(&input[..]));
}
