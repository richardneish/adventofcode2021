use std::collections::BTreeMap;
use std::io;

pub fn day_14a<R>(input_reader: R) -> i64
where
    R: io::BufRead,
{
    let (template, rules) = parse_input_v2(input_reader);

    let mut output = to_freq(&template);
    for step in 0..10 {
        output = apply_step_v2(output, &rules);
    }

    let last_char = template.chars().last().unwrap();
    count_element_freqs(output, last_char)
}

pub fn day_14b<R>(input_reader: R) -> i64
where
    R: io::BufRead,
{
    let (template, rules) = parse_input_v2(input_reader);

    let mut output = to_freq(&template);
    for step in 0..40 {
        output = apply_step_v2(output, &rules);
    }

    let last_char = template.chars().last().unwrap();
    count_element_freqs(output, last_char)
}

fn parse_input<R>(input_reader: R) -> (String, BTreeMap<String, String>)
where
    R: io::BufRead,
{
    let lines = input_reader.lines().map(|l| l.unwrap());
    let mut template = String::new();
    let mut rules: BTreeMap<String, String> = BTreeMap::new();
    let mut first_line = true;
    for line in lines {
        let line = line.trim();
        if first_line {
            template = String::from(line);
            first_line = false;
            continue;
        }
        if line.is_empty() {
            continue;
        }
        let parts: Vec<String> = line.split(" -> ").map(String::from).collect();
        rules.insert(
            parts.get(0).unwrap().to_string(),
            parts.get(1).unwrap().to_string(),
        );
    }

    (template, rules)
}

fn parse_input_v2<R>(input_reader: R) -> (String, BTreeMap<(char, char), char>)
where
    R: io::BufRead,
{
    let lines = input_reader.lines().map(|l| l.unwrap());
    let mut template = String::new();
    let mut rules: BTreeMap<(char, char), char> = BTreeMap::new();
    let mut first_line = true;
    let mut last_char = ' ';
    for line in lines {
        println!("line: {:?}", line);
        let line = line.trim();
        if first_line {
            template = String::from(line);
            first_line = false;
            continue;
        }
        if line.is_empty() {
            continue;
        }
        let parts: Vec<String> = line.split(" -> ").map(String::from).collect();
        let from: Vec<char> = parts.get(0).unwrap().to_string().chars().collect();
        let insert_str = parts.get(1).unwrap().to_string();
        rules.insert(
            (*from.get(0).unwrap(), *from.get(1).unwrap()),
            insert_str.chars().next().unwrap(),
        );
    }

    (template, rules)
}

fn apply_step_v2(
    mut freq: BTreeMap<(char, char), i64>,
    rules: &BTreeMap<(char, char), char>,
) -> BTreeMap<(char, char), i64> {
    let mut freq_updates: Vec<(((char, char), i64), (char, char), (char, char))> = Vec::new();
    for (pair, pair_freq) in &freq {
        if let Some(inserted_char) = rules.get(&pair) {
            //            println!("Inserting {:?} at position {:?}", insertion, insertion_pos);
            let first_pair = (pair.0, *inserted_char);
            let second_pair = (*inserted_char, pair.1);
            freq_updates.push(((*pair, *pair_freq), first_pair, second_pair));
        }
    }

    for ((pair, pair_freq), first_pair, second_pair) in freq_updates {
        //        println!(
        //            "apply_step_v2: {:?}, {:?}, {:?}",
        //            pair, first_pair, second_pair
        //        );
        adjust_freq(&mut freq, pair, -1 * pair_freq);
        adjust_freq(&mut freq, first_pair, pair_freq);
        adjust_freq(&mut freq, second_pair, pair_freq);
    }

    freq
}

fn adjust_freq(freq: &mut BTreeMap<(char, char), i64>, pair: (char, char), adjustment: i64) {
    let mut value = *(freq.get(&pair).unwrap_or(&0i64));
    value += adjustment;
    if value == 0i64 {
        freq.remove(&pair);
    } else {
        freq.insert(pair, value);
    }

    ()
}

fn adjust_ch_freq(freq: &mut BTreeMap<char, i64>, pair: char, adjustment: i64) {
    let mut value = *(freq.get(&pair).unwrap_or(&0i64));
    value += adjustment;
    if value == 0i64 {
        freq.remove(&pair);
    } else {
        freq.insert(pair, value);
    }

    ()
}

fn create_freq(input: &str) -> BTreeMap<(char, char), i64> {
    let mut output: BTreeMap<(char, char), i64> = BTreeMap::new();

    let mut insertion_pos = 1;
    for pos in 0..input.len() - 1 {
        let pair: Vec<char> = input.get(pos..=pos + 1).unwrap().chars().collect();
        //        println!("Examining pair '{:?}'", pair);
        output.insert((*pair.get(0).unwrap(), *pair.get(1).unwrap()), 0);
    }

    output
}

fn apply_step(input: &str, rules: &BTreeMap<String, String>) -> String {
    let mut output = String::from(input);

    let mut insertion_pos = 1;
    for pos in 0..input.len() - 1 {
        let pair = input.get(pos..=pos + 1).unwrap();
        //        println!("Examining pair '{:?}'", pair);
        if let Some(insertion) = rules.get(pair) {
            //            println!("Inserting {:?} at position {:?}", insertion, insertion_pos);
            output.insert_str(insertion_pos, insertion);
            insertion_pos += insertion.len();
        }
        insertion_pos += 1;
    }

    output
}

fn count_elements(input: String) -> i32 {
    let mut freq: BTreeMap<char, i32> = BTreeMap::new();
    for ch in input.chars() {
        let ch_freq = freq.get(&ch).unwrap_or(&0);
        freq.insert(ch, ch_freq + 1);
    }

    let mut least_common = (' ', i32::MAX);
    let mut most_common = (' ', 0);
    for (ch, ch_freq) in freq {
        if ch_freq > most_common.1 {
            most_common = (ch, ch_freq);
        }
        if ch_freq < least_common.1 {
            least_common = (ch, ch_freq);
        }
    }

    most_common.1 - least_common.1
}

fn count_element_freqs(pair_freqs: BTreeMap<(char, char), i64>, last_char: char) -> i64 {
    let mut freq: BTreeMap<char, i64> = BTreeMap::new();
    for ((ch1, ch2), pair_freq) in pair_freqs {
        adjust_ch_freq(&mut freq, ch1, pair_freq);
        //     adjust_ch_freq(&mut freq, ch2, pair_freq);
    }

    let mut least_common = (' ', i64::MAX);
    let mut most_common = (' ', 0i64);
    for (ch, mut ch_freq) in freq {
        if ch == last_char {
            ch_freq = ch_freq + 1;
        }
        println!("char '{:?}' appears {:?} times", ch, ch_freq);
        if ch_freq > most_common.1 {
            most_common = (ch, ch_freq);
        }
        if ch_freq < least_common.1 {
            least_common = (ch, ch_freq);
        }
    }

    most_common.1 - least_common.1
}

fn to_freq(input: &str) -> BTreeMap<(char, char), i64> {
    let mut result: BTreeMap<(char, char), i64> = BTreeMap::new();

    for pos in 0..input.len() - 1 {
        let pair = input.get(pos..=pos + 1).unwrap();
        adjust_freq(
            &mut result,
            (pair.chars().nth(0).unwrap(), pair.chars().nth(1).unwrap()),
            1,
        );
    }
    // println!("to_freq({:?}) = {:?}", input, result);
    result
}

#[test]
fn test_parse_input() {
    let input = b"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
    let template = String::from("NNCB");
    let rules = BTreeMap::from([
        (('C', 'H'), 'B'),
        (('H', 'H'), 'N'),
        (('C', 'B'), 'H'),
        (('N', 'H'), 'C'),
        (('H', 'B'), 'C'),
        (('H', 'C'), 'B'),
        (('H', 'N'), 'C'),
        (('N', 'N'), 'C'),
        (('B', 'H'), 'H'),
        (('N', 'C'), 'B'),
        (('N', 'B'), 'B'),
        (('B', 'N'), 'B'),
        (('B', 'B'), 'N'),
        (('B', 'C'), 'B'),
        (('C', 'C'), 'N'),
        (('C', 'N'), 'C'),
    ]);
    assert_eq!((template, rules), parse_input_v2(&input[..]));
}

#[test]
fn test_single_step_simple() {
    let template = to_freq("NNC");
    let step1_output = to_freq("NCNBC");
    let rules = BTreeMap::from([
        (('C', 'H'), 'B'),
        (('H', 'H'), 'N'),
        (('C', 'B'), 'H'),
        (('N', 'H'), 'C'),
        (('H', 'B'), 'C'),
        (('H', 'C'), 'B'),
        (('H', 'N'), 'C'),
        (('N', 'N'), 'C'),
        (('B', 'H'), 'H'),
        (('N', 'C'), 'B'),
        (('N', 'B'), 'B'),
        (('B', 'N'), 'B'),
        (('B', 'B'), 'N'),
        (('B', 'C'), 'B'),
        (('C', 'C'), 'N'),
        (('C', 'N'), 'C'),
    ]);
    let result = apply_step_v2(template, &rules);
    assert_eq!(step1_output, result);
    count_element_freqs(result, 'C');
}

#[test]
fn test_single_step() {
    let template = to_freq("NNCB");
    let step1_output = to_freq("NCNBCHB");
    let step2_output = to_freq("NBCCNBBBCBHCB");
    let step3_output = to_freq("NBBBCNCCNBBNBNBBCHBHHBCHB");
    let step4_output = to_freq("NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB");
    let rules = BTreeMap::from([
        (('C', 'H'), 'B'),
        (('H', 'H'), 'N'),
        (('C', 'B'), 'H'),
        (('N', 'H'), 'C'),
        (('H', 'B'), 'C'),
        (('H', 'C'), 'B'),
        (('H', 'N'), 'C'),
        (('N', 'N'), 'C'),
        (('B', 'H'), 'H'),
        (('N', 'C'), 'B'),
        (('N', 'B'), 'B'),
        (('B', 'N'), 'B'),
        (('B', 'B'), 'N'),
        (('B', 'C'), 'B'),
        (('C', 'C'), 'N'),
        (('C', 'N'), 'C'),
    ]);
    assert_eq!(step1_output, apply_step_v2(template, &rules));
    assert_eq!(step2_output, apply_step_v2(step1_output, &rules));
    assert_eq!(step3_output, apply_step_v2(step2_output, &rules));
    assert_eq!(step4_output, apply_step_v2(step3_output, &rules));
}

#[test]
fn test_day_14a() {
    let input = b"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
    assert_eq!(1588, day_14a(&input[..]));
}

#[test]
fn test_day_14b() {
    let input = b"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
    assert_eq!(2188189693529i64, day_14b(&input[..]));
}
